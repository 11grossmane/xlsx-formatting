# XLSX Functions

## Installation

clone this repository then, cd into it then:

if you don't already have pipenv installed:

```bash
brew install pipenv
```

then:

```bash
pipenv install
pipenv shell #if you still need to enter shell
```

## Usage

### To remove whitespace:

```bash
python remove-trailing.py '<path-to-source.xlsx>' '<path-to-destination.xlsx>'
```

### To remove strange characters:

```bash
python remove-strange-chars.py '<path-to-source.xlsx>' '<path-to-destination.xlsx>'
```

### to convert NoneType to empty string:

```bash
python none-to-empty-string.py '<path-to-source.xlsx>' '<path-to-destination.xlsx>'
```
