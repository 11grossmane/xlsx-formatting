from openpyxl import load_workbook
import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument("sourcePath", help="source path")
parser.add_argument("destinationPath", help="destination path")
args = parser.parse_args()


def removeStrangeChars(filePath, destinationPath):
    wb = load_workbook(filename=filePath)
    ws = wb.active
    for row in ws:
        for cell in row:
            str = ''
            det = False
            for idx, char in enumerate(cell.value):
                if re.match('[\w,&/ .-]', char):
                    str += char
                else:
                    print(char)
                    det = True
                if idx+1 == len(cell.value):
                    cell.value = str
                    if det == True:
                        print(cell.value)
    wb.save(destinationPath)


removeStrangeChars(args.sourcePath, args.destinationPath)
