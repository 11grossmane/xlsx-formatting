from openpyxl import load_workbook
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('sourcePath', help='source path')
parser.add_argument('destinationPath', help='destination path')
args = parser.parse_args()


def noneToEmpty(filePath, destinationPath):
    wb = load_workbook(filename=filePath)
    ws = wb.active
    for row in ws:
        for cell in row:
            if cell.value is None:
                cell.value = ''
    wb.save(destinationPath)


noneToEmpty(args.sourcePath, args.destinationPath)
